EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Connectors
LIBS:Switches
LIBS:STM32F
LIBS:Tag-Connect
LIBS:LEDs
LIBS:Memory
LIBS:PMIC
LIBS:Interface
LIBS:Transistors
LIBS:PocketReperfRotaryPCB-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "PocketReperf Rotary Switch Adapter PCB"
Date ""
Rev "1"
Comp "NF6X"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 6400 3750
NoConn ~ 6400 3650
$Comp
L CONN_02X06 J1
U 1 1 5714A169
P 5900 4650
F 0 "J1" V 5900 5050 50  0000 C CNN
F 1 "CONN_02X06" H 5900 4300 50  0000 C CNN
F 2 "NF6X_Connectors:Molex_878311241" H 5900 3450 60  0001 C CNN
F 3 "" H 5900 3450 60  0000 C CNN
F 4 "Molex" V 5900 4650 60  0001 C CNN "manf"
F 5 "878311241" V 5900 4650 60  0001 C CNN "manf#"
	1    5900 4650
	0    -1   1    0   
$EndComp
Wire Wire Line
	5900 3950 5900 4300
Wire Wire Line
	5850 4300 5950 4300
Wire Wire Line
	5850 4300 5850 4400
Wire Wire Line
	5950 4300 5950 4400
Connection ~ 5900 4300
Wire Wire Line
	5400 3750 5400 3900
Wire Wire Line
	5400 3900 5750 3900
Wire Wire Line
	5750 3900 5750 4400
Wire Wire Line
	5400 3650 5300 3650
Wire Wire Line
	5300 3650 5300 4000
Wire Wire Line
	5300 4000 5650 4000
Wire Wire Line
	5650 4000 5650 4400
Wire Wire Line
	6050 4400 6050 3900
Wire Wire Line
	6050 3900 6500 3900
Wire Wire Line
	6500 3900 6500 3550
Wire Wire Line
	6500 3550 6400 3550
$Comp
L NKK_MRA112 S1
U 1 1 57149A68
P 5900 3450
F 0 "S1" H 5900 3950 60  0000 C CNN
F 1 "NKK_MRA112" H 5900 3850 60  0000 C CNN
F 2 "NF6X_Switches:NKK_MRA112" H 6000 2850 60  0001 L CNN
F 3 "" H 5900 3450 60  0000 C CNN
F 4 "NKK" H 6000 3050 60  0001 L CNN "manf"
F 5 "MRA112" H 6000 2950 60  0001 L CNN "manf#"
	1    5900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 4400 6150 4000
Wire Wire Line
	6150 4000 6600 4000
Wire Wire Line
	6600 4000 6600 3350
Wire Wire Line
	6600 3350 6400 3350
Wire Wire Line
	6400 3250 6700 3250
Wire Wire Line
	6700 3250 6700 5000
Wire Wire Line
	6700 5000 6150 5000
Wire Wire Line
	6150 5000 6150 4900
Wire Wire Line
	6050 4900 6050 5100
Wire Wire Line
	6050 5100 6800 5100
Wire Wire Line
	6800 5100 6800 3150
Wire Wire Line
	6800 3150 6400 3150
Wire Wire Line
	5400 3550 5200 3550
Wire Wire Line
	5200 3550 5200 5000
Wire Wire Line
	5200 5000 5650 5000
Wire Wire Line
	5650 5000 5650 4900
Wire Wire Line
	5400 3350 5100 3350
Wire Wire Line
	5100 3350 5100 5100
Wire Wire Line
	5100 5100 5750 5100
Wire Wire Line
	5750 5100 5750 4900
Wire Wire Line
	5400 3250 5000 3250
Wire Wire Line
	5000 3250 5000 5200
Wire Wire Line
	5000 5200 5850 5200
Wire Wire Line
	5850 5200 5850 4900
Wire Wire Line
	5400 3150 4900 3150
Wire Wire Line
	4900 3150 4900 5300
Wire Wire Line
	4900 5300 5950 5300
Wire Wire Line
	5950 5300 5950 4900
$EndSCHEMATC
