# PocketReperf: A Teletype Punch/Reperforator Emulator that Fits in your (large) Pocket

## Description

PocketReperf is an accessory for users of vintage teletype equipment using 20mA or 60mA high voltage (i.e., up to 125VDC) current loop interfaces and 5-bit coding. Its primary motivation is to stand in for a paper tape punch/reperforator, such as the US military model TT-76 made by Kleinschmidt. As such, PocketReperf allows a teletype user to record and play back multiple messages stored in nonvolatile memory. It records and plays back up to eight virtual tapes of up to 16,382 (16k - 2) characters each.

In addition, PocketReperf also provides a a USB interface to allow modern computers to be interfaced to vintage teletype equipment. PocketReperf does not include a current loop power supply, however, so loop current will need to be provided by another instrument in each loop (one loop in half duplex configurations, or two loops in full duplex configurations).

PocketReperf is small enough to fit in most cargo pockets.

## Project Status

PocketReperf is in an early stage of development. It doesn't actually exist yet.

## Features

* Supports 20mA and 60mA high-voltage loops without reconfiguration.

* Separate receiver (Rx) and transmit (Tx) loop connections via 1/4" phone jacks. Two jacks per loop. Rx jacks are color-coded red.

* Switchable full duplex (FDX) or half duplex (HDX) configuration. In HDX configuration, the Rx and Tx jacks are in series. In FDX configuration, the Rx and Tx loops are not internally connected together, though they may still be connected externally.

* Can be powered from 60mA loop current via the Tx jacks, allowing use as an emulated punch/reperforator without any separate power source. Receiving power from a 20mA loop is desirable, but a 20mA loop may not provide enough power. Power supply from the Tx loop may be disabled to lower PocketReperf's burden voltage on the Tx loop. When disabled, PocketReperf must receive power from its USB interface.

* Also can be powered from USB. No reconfiguration is needed. USB-sourced and loop-sourced power are diode-isolated.

* Rx and Tx loops are galvanically isolated from the USB interface, and also from each other when in FDX configuration.

* Switchable 60WPM (45.45 baud) or 100WPM (75 baud) operation.

* USB interface may be configured for either raw 5-bit transfer or translation between 5-bit coding on the current loops and ASCII coding on the USB interface.

* USB interface presents a USB CDC class interface, i.e. a virtual COM port. All configuration settings from the USB host such as baud rate, bits per character, handshaking mode, etc. are ignored.

* Records up to 8 separate virtual tapes from the Rx interface in nonvolatile memory.

* Plays back up to 8 virtual tapes via the Tx interface, plus two pre-defined repeating test sequences: RYRY... with CR-LF-LTRS after each 60 characters, and an A-Z sequence in both letters and figures shifts. 

* Recording messages from the USB interface requires connection of an external loop supply, and connection of the Rx and Tx interfaces together, either by switching to HDX mode or with external cabling.

* Unit is not damaged by connection of loop interfaces with polarity reversed.

* Each loop interface is protected with a self-resetting thermal fuse.

* LED indicators show presence of Rx and Tx current.

* Unit is installed in an extruded aluminum Hammond box.

* Loop interface phone jack sleeves are isolated from the case for safety purposes. User must still exercise caution when plugging and unplugging cables with loop power applied. Remember that electrical safety conventions were a lot more lax back in the days of 60mA high voltage current loops! It is highly recommended to shut off the loop power supply before plugging or unplugging any current loop interface connectors.

* Processor (STM32L073RZT6) may be reprogrammed via Device Firmware Update (DFU) mode over the USB interface. A normal/DFU boot mode switch and a reset button are provided on the bottom of the case.


## Artist's Renditions

### Front

![](docs/PocketReperf Front Rendering.png?raw=true)

### Rear

![](docs/PocketReperf Rear Rendering.png?raw=true)


## Controls, Indicators and Connections

### Diagrams

[Diagram of Front Panel Controls, Connectors and Indicators](docs/PocketReperf Front Panel.pdf)

[Diagram of Rear Panel Controls and Connectors](docs/PocketReperf Rear Panel.pdf)

[Diagram of Bottom Panel Controls](docs/PocketReperf Bottom Panel.pdf)

### Wall of Text

* USB type B jack providing USB 2.0 FS device interface.

* Play/Stop push button, color coded black.

* Knob to select from 8 virtual tape loops and two pre-defined repeating test sequences.

* Record push button, color coded red. To record, press and release Play/Stop while holding Record. Press and release record to finish recording.

* Toggle switch to select 60 WPM (45.45 baud) or 100 WPM (75 baud) on the current loop interfaces.

* Toggle switch to select raw 5-bit coding or ASCII translation on the USB interface.

* Green LED over USB jack indicating Tx loop current.

* Red LED over USB jack indicating Rx loop current.

* Green LED over Play/Stop button indicating playback is in progress.

* Red LED over Record button indicating recording is in progress.

* Two 1/4" phone jacks in series for Rx loop, color-coded red. Tip positive, sleeve negative. Each jack is switched to automatically short when no plug is inserted.

* Two 1/4" phone jacks in series for Tx loop, color-coded black. Tip positive, sleeve negative. Each jack is switched to automatically short when no plug is inserted.

* Toggle switch to select FDX or HDX configuration.

* Slide switch concealed on bottom to write-protect all virtual tape loops.

* Slide switch concealed on bottom to select normal or DFU (firmware update) boot mode.

* Reset button concealed on bottom.
